# Python


## Chapter 1: Intro (python and your OS)

Basically this covers how python and your OS interact with each other and how to make sure they do so smoothly. There's more `bash` code here than python code. It's probably good to type along with the code snippets.

### Install Python

First things first: there are probably multiple versions of python already installed on your computer. If you 

    $ ls /usr/bin/

and scroll around a bit, you should see a cluster of programs that start with `python` (I use `$` to indicate a command executed in the bash **REPL**). The important ones are `python`, `python2` and `python3`. Why so many? Well basically both as a program and as a language Python version 2 is a bit different from python versions 3.X.X and higher. Importantly it's different enough that programs written in `python3.x` probably cannot be run in `python2.x` and vise versa.  Because of this, the two are treated somewhat differently, often like entirely different programs. For instance, if you run

    $ python --version

right now, chances are, you'll probably see something like

    $ Python 2.7.12

This is because lots of OS's come with a version of python version 2 already installed. Now try running

    $ python3 --version

Chances are you got something different. Probably something like

    $ Python 3.5.2

These two programs, `python` and `python3` are just different versions of python, but they get treated as different programs in (as far as I know) all cases. 

**Note:** your system actually probably relies on all three programs: `python`, `python2` and `python3` (for instance, the `gnome-terminal` relies on `python3`). So for the love of all that is holy *don't* mess with any of them any more than you have to. If any are missing go ahead and install, but be very careful modifying anything that's there already. I personally learned this the hard way. 

### What is PIP?

There are a lot of programs written in python. Some of these are actually python-language software libraries. Before you can use any of these though, you'll have to install them. Installing lots of diverse packages manually could get quite tedious though...

PIP is a python language *package manager*: a program that automates the process of installing and otherwise initializing python-language programs on remote machines (like your trusty ol' computer).

You can check if you have it installed:

    $ pip -V

or rather, for the python3 version:

    $ pip3 -V

because python2 and python3 use different package managers, much like entirely different programs would.

### Virtual Environments

Managing lots of different python packages for different projects can get very messy very quick, even without the added complexity of having multiple discrete python versions to worry about. 

*Virtual environments* are the most common solution to this: basically a self-contained directory with its own installations of `python`, `python3` and even `PIP` and `PIP3`. The idea is to have one of these *virtual environments* (hereafter VE) within the root directory of every python project. You then install all the required python packages for that project (or even different python versions) inside the VE. This way your computer's global python environment stays clean and different projects can have different dependencies without buggering each other up.

There are (probably) a bunch of tools for creating VE's but the most reliable one seems to be a project called `virtualenv` that is itself a python-language program, installable through PIP. Confirm it's installed properly with 

    $ irtualenv --version

Great! You should now be able to set up a VE anywhere on your computer. By convention though the VE should be called `env` and located in the root of your project:

    $ d newProject
    $ irtualenv env

`./env` is now a python VE. It should contain a bunch of auto-generated directories, as well as installed versions of `python` and `python3`. There's one more step you might want to before you start coding though:

    $ ource env/bin/activate

What this does is run an auto-generated script that **temporarily** prepends the path to the current virtual environment's `bin/` directory to the start of `$PATH` (so now the VE's packages get searched first). It it also modifies some `ENV` variables, but that's neither here nor there. The altered `$PATH` only lasts as long as the current terminal session though. You can return the path to normal with

    $ eactivate `<<< no bin`

Now we can start installing python dependencies, try it out:

    $ pip install requests

If everything is working correctly and your path is `activate`'d properly, this should create a version of the `requests` package in `./env/lib/your-python3-version/site-packages/requests`. We'll talk about how to use this package next.


### Actual Code

Ok, all well and good, we have a new project with its own quarantined python VE, let's actually write something. Go ahead and start a python3 **REPL** in your terminal:

    $ python3

A bunch of text looking something like this:

    Python 3.5.2 (default, Nov 23 2017, 16:37:01) 
    [GCC 5.4.0 20160609] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    
should pop up, and now the input you give to the terminal should be interpreted and executed in/by python (specifically the python3 program installed in your local VE), rather than trusty old bash. Lets try it out (from now on `>>>` indicates that we're running code in the python3 REPL and `=>` indicates a return value)!

    >>> 'anus' # => 'anus'
    >>> print('anus') # => anus
    >>> 'anus' + 'anus" # => 'anusanus'

Good stuff. One last step before we dive in though: how to use python packages in your code. Once we know how to do that properly we can pretty much stop worrying about the python <-> OS interactions and just focus on all that juicy juicy machine learning or whatever. 

You can exit the python3 REPL and return to bash with `ctrl+d`.

### Importing Things

Ok so first off, lets make it fail. Spin up the python3 REPL again if you shut it off and lets try to import something that doesn't exist:

    >>> import nonexistantfilethatdoesntexist

In response your python REPL *should* should spit out an error that looks like this:

    Traceback (most recent call last):
        File "<stdin>", line 1, in <module>
    ImportError: No module named 'nonexistantfilethatdoesntexist'

If it *didn't* then it might mean your REPL actually *found* a python file with that absurd name, which is, if anything, probably more troubling. Let's make the safe assumption that we got the above error though: what actually just happened? 

Whenever python3 executes an `import some_file_name` statement, the following things happen:

1. python3 searches for a **module** named `some_file_name.py`
2. If the search succeeds, python3 "includes" the module `some_file_name` for use in the current environment (more on this later).

Let's break this down.

#### 1. Search for module

A module is just any python file containing **definitions** or **statements**. These are technical python terms that we'll get to later, but basically any file containing variable or function definitions fits the bill.

Python always searches for modules in the following places, in the following order:

1. python's record of **built-in modules** (i.e. modules that come pre-packaged with the language, like `sys` or `math`)
2. The directory containing the python file currently being executed, or if there is no specified current file (as in the case of commands executed in a REPL), the current directory
3. A pre-defined list of directories called `PYTHONPATH`
4. The **installation-dependent default path**, which is the place where the global `python` program was installed by default (probably like `/usr/local/lib/python) or something.

As soon as it finds a module with a matching name, it considers the search a success. If, on the other hand, it searches all 4 locations and finds no matches, it throws an error, just like it did for us. 

A useful side-note: `sys.path` (`sys` being a build-in module) points to a python **list** that contains all the directories that will be searched by an `import` statement, in the corret order.

    >>> import sys
    >>> sys.path # will return a long python list

Let's make the import fail again, but with a more reasonable module search:

    >>> import testimport

This almost certainly spat out an error too because most of the directories searched by `import` are pretty darn empty.

#### 2. Module Inclusion

Ok, now let's make the import succeed. The easiest way to do this is to import a built-in module like we did earlier with `sys`, but that's not very instructive. Instead let's make our own module and import it. This is actually very simple too. Simply create a new file in the root of `newProject` (or whatever you named your project file) called `testimport.py` and insert the following code:

```python
# testimport.py

name = 'Zdzislaw Beksinski'
```

Now go back into the python3 REPL (making sure you open it inside `newProject`) and try importing that module again

    >>> import testimport # should throw no errors

Progress! Apparently `import`'s search step was able to locate our newly created module. This makes sense because we know the second place `import` searches is the current directory, which will be `newProject` if we started our python REPL in the right place.

 Ok but what *actually* happened? When a successfully located module called say `testimport` is "imported" into the current python environment a bunch of things happen in order. Without getting too technical:

1. Every line of code in that module is executed
2. If `1.` didn't raise any errors, a "Module object" is created, containing all the variables and functions defined in `testimport`'s global, or top-level scope.
3. This object is stored in various places in the current environment, including the current global or top-level scope

If you're keen to get a bit more technical, at the time of writing there's a really good in-depth explanation of this process sitting at https://docs.python.org/2.0/ext/methodTable.html.

In any case, the substantive consequence of all this is that since `import testimport` succeeded, we should now have a `testimport` object sitting at the top level of our REPL's environment for our convenience. 

    >>> testimport # => <module 'testimport' from '/home/lewington/code/python/learn/myproject/testimport.py'>

more importantly, we should now be able to access the variables defined in that module through that object:

    >>> testimport.name # => 'Zdzislaw Beksinski'

Boom. Feel free to experiment by adding more variables or maybe even functions to `testimport.py`. Keep in mind though that the import is only successful because `testimport.py` is on python's search path (which again, is stored under `sys.path`). As soon as you move `testimport.py` off that path, attempts to import it will throw errors again.

If you're desperate you can modify the python import path, but that's advanced shit. For now we'll just cover one more topic before coding in earnest: importing packages. 

#### Importing packages

In python a **package** is actually a special type of module. The technical definition of a package in python is any module that contains a `__path__` attribute, but don't worry too much about that. There are different kinds of packages, but the only kind we're going to cover here are **Regular Packages**. These generally take the form of a directory (any directory) containing a file with the name of `__init__.py`. 

Importing regular packages works a lot like importing normal modules; every line in `__init__.py` is executed and any variables defined in the process are included in the resulting module object, which is itself included in the top-level of the current environment. Lets give it a whirl.

1. create a new directory in the root of your project called `tonyabbot`. 
2. create a new file inside `tonyabbot` called `__init__.py` and insert the following code:


```python
# tonyabbot/__init__.py

lunch = 'delicious onion'
```

Your filesystem should now look something like this:

    +-- newProject
    |   +-- testimport.py
    |   +-- env
    |       +-- ...
    |
    |   +-- tonyabbot
    |       +-- __init__.py
    |

Checks out? Great! Congratulations, you've just made your first package. We can now import it just like we imported `testimport.py`:

    >>> import tonyabbot # no errors... right?
    >>> tonyabbot.lunch # => 'delicious onion'

Now, in the wild you'd probably have a whole bunch of modules, probably even sub-packages sitting inside `tonyabbot`. Some of which would then be imported and used inside `__init__.py` to make the `tonyabbot` expose some legitimately useful functionality when you imported it, but we'll get ambitious later yeah?

The *import*-ant (ho-HO!) point is that the `requests` package we installed waaaaaaaay back when using

    $ pip install requests

inside out VM is just a package, more complicated by several orders of magnitude than our humble `tonyabbot` package, but fundamentally of the same kind. If you go ahead and look at `/myProject/env/lib/my-python-version/site-packages/requests` you'll see an `__init__.py` module (at the time of writing at least).When we attempt to import `requests`, exactly the same process occurs as when we import `tonyabbot`, the only difference is that we get useful functionality instead of just the `lunch` variable. 

So spin up that python3 REPL again, but before we actually do the import lets just confirm that the `import` function is going to be able to *find* the `requests` package. 

    >>> import sys
    >>> sys.path # => a really long list of directories

Have a good look at that really long list of directories, remembering that this is the complete list of all directories that `import modulename` will search for `modulename.py` before giving up and throwing an error. 

Did you find a path ending in `...myProject/env/lib/my-python-version/site-packages`? If you're running a version of python stored inside `myProject/env` you should see it somewhere or other, since these are set up to automatically search for packages in the local `site-packages` directory before trying to access globally installed packages.

If so, great! that's where `requests` is stored, so now we know for sure that `import requests` is going to work, and we also know why. Lets give it a whirl by trying out the incredibly helpful `requests.get()` method, which lets you send http requests to any website super easily.

    >>> import requests # no errors?
    >>> requests.get('http://bing.com') # => <Response [200]>

Nice! We got a response from my favorite website and `requests` bundled that response into a handy object for us to use later. You can read more about python's `requests` package online if you're keen. The rest of us are going to do a quick recap of what we've learned this chapter and then collapse from exhaustion.

### Recap

1. There are different versions of python that act like different programs. 
2. You probably have some of these versions installed for global on your system already, but you shouldn't really ever use these global versions in your projects. Instead you should create self-contained projects, each relying on their own quarantined python **Virtual Environment**.
3. You'll probably be using a lot of python **packages** to help you create any given project. These should be installed inside your project's **Virtual Environment**, probably using **PIP** (which automatically installs them in the `site-packages` directory).
4. Python **packages** are just fancy python **modules**.
5. You can include any python module in your current code using the `import modulename` command. 
6. What this command actually does is search a pre-defined search-path for a module called `modulename.py`, create a "Module object" from the module, and include that object in the current environment (or throw an error if the module can't be found). 

Phew.